using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour
{
    // se encarga de diversas cosa como la pausa, el reinicio de partida, salir al menu principal y quien gana el juego
    public KeyCode pausa1, pausa2;
    static public string nombre;
    public TextMeshProUGUI textGanador;
    public GameObject pausaObjeto;

    private void Start()
    {
        textGanador.gameObject.SetActive(false);
        Time.timeScale = 1;
        nombre = "";
        pausaObjeto.SetActive(false);
        GestorDeAudio.instancia.PararSonido("Ganar");
        GestorDeAudio.instancia.ReproducirSonido("Carrera");
    }

    private void Update()
    {
        pausa();
        ganador();
    }

    void pausa()// pausa el juego
    {
        if ((Input.GetKeyDown(pausa1) || Input.GetKeyDown(pausa2)) && nombre == "")
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                GestorDeAudio.instancia.PausarSonido("Carrera");
            }
            else
            {
                Time.timeScale = 1;
                GestorDeAudio.instancia.ReproducirSonido("Carrera");
            }
            pausaObjeto.SetActive(!pausaObjeto.activeSelf);
        }
    }

    void ganador()// informa que se termino el juego y quien lo gano
    {
        if (nombre != "")
        {
            GestorDeAudio.instancia.PararSonido("Carrera");
            GestorDeAudio.instancia.ReproducirSonido("Ganar");
            textGanador.gameObject.SetActive(true);
            textGanador.text = nombre;
            Time.timeScale = 0;
        }
    }

    public void reiniciar()// reinicia la esena/partida
    {
        GestorDeAudio.instancia.PararSonido("Ganar");
        SceneManager.LoadScene(1);
    }

    public void salir()// vuelve al menu inicio
    {
        GestorDeAudio.instancia.PararSonido("Ganar");
        SceneManager.LoadScene(0);
    }
}
