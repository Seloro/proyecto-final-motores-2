using UnityEngine;
[System.Serializable]
public class Sonido
{
    // clase sonido que se utilica en el gestor de sonido
    public string Nombre;
    public AudioClip ClipSonido;
    [Range(0f, 1f)]
    public float Volumen;
    [Range(0f, 3f)]
    public float pitch;
    public bool repetir;
    [HideInInspector]
    public AudioSource FuenteAudio;
}
