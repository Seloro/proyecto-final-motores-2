using System;
using UnityEngine;
public class GestorDeAudio : MonoBehaviour
{
    //Utilizado para gestionar el sunido/musica del juego
    public Sonido[] sonidos;
    public static GestorDeAudio instancia;
    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        foreach (Sonido s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.loop = s.repetir;
        }
        //agrega al game objet que tenga el codigo tantos audio sources como alla en la lista de objetos de clase sonido y les pasa los valores de: clip, volumen, pitch, loop
    }

    public void ReproducirSonido(string nombre)// reproduce un sonido en funcion de un nombre (string) dado siempre que se tenga un audio source con ese nombre y no se este reproduciendo
    {
        Sonido s = Array.Find(sonidos, sound => sound.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            if (!s.FuenteAudio.isPlaying)
                s.FuenteAudio.Play();
        }
    }
    public void PausarSonido(string nombre)// Pausa la reproduccion de un sonido en funcion de un nombre (string) dado siempre que se tenga un audio source con ese nombre
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
        }
    }
    public void PararSonido(string nombre)// Detiene la reproduccion de un sonido en funcion de un nombre (string) dado siempre que se tenga un audio source con ese nombre
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Stop();
        }
    }
}