using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Parallax_Individual : MonoBehaviour
{
    // Mueve las capas del parallax a velocidades diferentes teniendo en cuenta la velocidad del jugador en el eje x (incluye la direccion a la que se mueve: si va a la derecha, el parallax va a la izquieda y viseversa)
    public Rigidbody rb;
    public float velocidad;

    private void Update()
    {
        if (Time.timeScale != 0)
            transform.localPosition += Vector3.right * (-rb.velocity.x / velocidad);

        if (math.abs(transform.localPosition.x) > 37.5f)
        {
            if (transform.localPosition.x > 0)
                transform.localPosition = new Vector3(-37.5f, transform.localPosition.y, transform.localPosition.z);
            else
                transform.localPosition = new Vector3(37.5f, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
