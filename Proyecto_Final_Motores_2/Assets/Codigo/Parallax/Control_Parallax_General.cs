using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Parallax_General : MonoBehaviour
{
    //Repocisiona el empty, que contiene las capas del parallax, en el eje x
    public GameObject jugadorObjetivo;

    private void Update()
    {
        transform.position = new Vector3( jugadorObjetivo.transform.position.x, transform.position.y, transform.position.z);
    }
}
