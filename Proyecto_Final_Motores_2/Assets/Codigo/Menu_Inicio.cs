using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Inicio : MonoBehaviour
{
    //Metodos de carga o cierre de juego para los botones del menu inicio

    public GameObject panelInicio, panelEleccion;
    public TextMeshProUGUI textoSector, textoNombre, siguienteJugar;
    public List<string> nombresSectoresElejidos = new List<string>();
    public List<string> nombresDeSectores = new List<string>();
    int indice;
    public int[] indicesAuciliares;

    private void Start()
    {
        Randomizador.sectoresElejidos = false;
        panelEleccion.SetActive(false);
    }
    public void jugar()
    {
        SceneManager.LoadScene(1);
    }
    public void salir()
    {
        Application.Quit();
    }
    public void camvioDePanel()
    {
        panelEleccion.SetActive(!panelEleccion.activeSelf);
        panelInicio.SetActive(!panelInicio.activeSelf);
        reinicio();
        indice = 0;
        textoNombre.text = nombresSectoresElejidos[indice];
        textoSector.text = "Sector: " + (indice + 1);
    }
    public void volver()
    {
        indice--;

        if (indice < 0)
            camvioDePanel();
        else
        {
            textoNombre.text = nombresSectoresElejidos[indice];
            textoSector.text = "Sector: " + (indice + 1);
            siguienteJugar.text = "Siguiente";
        }
    }
    public void cargarSector(string nombre)
    {
        for (int i = 0; i < nombresDeSectores.Count; i++)
        {
            if (nombresDeSectores[i] == nombre)
            {
                nombresSectoresElejidos[indice] = nombre;
                textoNombre.text = nombresSectoresElejidos[indice];
               indicesAuciliares[indice] = i;
            }
        }
    }
    public void jugarModoSeleccion()
    {
        indice++;

        if (indice > 3)
        {
            Randomizador.sectoresElejidos = true;
            Randomizador.indisesElejidos = indicesAuciliares;
            SceneManager.LoadScene(1);
        }
        else
        {
            textoNombre.text = nombresSectoresElejidos[indice];
            textoSector.text = "Sector: " + (indice + 1);
            if (indice == 3)
                siguienteJugar.text = "Jugar";
        }
    }
    void reinicio()
    {
        for(int i = 0; i < nombresSectoresElejidos.Count; i++)
        {
            nombresSectoresElejidos[i] = "------";
            indicesAuciliares[i] = 0;
        }
    }
}
