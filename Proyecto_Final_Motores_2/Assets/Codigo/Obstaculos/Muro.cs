using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muro : MonoBehaviour
{
    // destrulle el objeto cuando coliciona con un jugador

    public GameObject polvo;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Jugador 1")
                GestorDeAudio.instancia.ReproducirSonido("Muro1");
            else 
                GestorDeAudio.instancia.ReproducirSonido("Muro2");
            Instantiate(polvo, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Bala"))
        {
            Instantiate(polvo, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
