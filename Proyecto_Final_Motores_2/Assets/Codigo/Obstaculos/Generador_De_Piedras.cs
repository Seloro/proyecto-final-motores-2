using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador_De_Piedras : MonoBehaviour
{
    public GameObject piedra;
    float temp, tempMax;

    void Start()
    {
        Instantiate(piedra, gameObject.transform.position, Quaternion.identity);
        tempMax = Random.Range(1, 5);
    }

    void Update()
    {
        regenerar();
    }

    void regenerar()
    {
        if (temp < tempMax)
            temp += Time.deltaTime;
        else
        {
            Instantiate(piedra, gameObject.transform.position, Quaternion.identity);
            temp = 0;
            tempMax = Random.Range(1, 5);
        }
    }
}
