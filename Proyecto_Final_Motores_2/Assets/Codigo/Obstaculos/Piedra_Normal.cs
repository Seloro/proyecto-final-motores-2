using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piedra_Normal : MonoBehaviour
{

    public GameObject polvo;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.mass = Random.Range(0, 7);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(polvo, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
