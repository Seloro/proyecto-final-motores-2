using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma_Rapida : MonoBehaviour
{
    Material mat;
    public Color colorJugador1, colorJugador2;
    public bool aceleraAJugador1, random, cambiante, cambioRandomizado;
    static public bool acelerarJugador1, acelerarJugador2;
    string nombre;
    float temp;
    public float tempMax = 10;
    GameObject jugador1, jugador2;

    void Start()
    {
        mat = GetComponent<Renderer>().material;
        acelerarJugador1 = false;
        acelerarJugador2 = false;

        if (!random)
        {
            if (aceleraAJugador1)
            {
                mat.color = colorJugador1;
                nombre = "Jugador 1";
            }
            else
            {
                mat.color = colorJugador2;
                nombre = "Jugador 2";
            }
        }
        else
            randomizacion();
    }

    private void Update()
    {
        comprobacion();
        cambio();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Jugador 1")
                jugador1 = collision.gameObject;
            else
                jugador2 = collision.gameObject;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Jugador 1")
            {
                jugador1 = null;
                acelerarJugador1 = false;
            }
            else
            {
                jugador2 = null;
                acelerarJugador2 = false;
            }
        }
    }

    void randomizacion()
    {
        if (Random.Range(0, 2) == 0)
        {
            mat.color = colorJugador1;
            nombre = "Jugador 1";
        }
        else
        {
            mat.color = colorJugador2;
            nombre = "Jugador 2";
        }
    }

    void cambio()
    {
        if (cambiante)
        {
            if (temp < tempMax)
                temp += Time.deltaTime;
            else
            {
                temp = 0;
                if (!cambioRandomizado)
                {
                    if (nombre == "Jugador 1")
                    {
                        mat.color = colorJugador2;
                        nombre = "Jugador 2";
                    }
                    else
                    {
                        mat.color = colorJugador1;
                        nombre = "Jugador 1";
                    }
                }
                else
                {
                    randomizacion();
                }
            }
        }
    }

    void comprobacion()
    {
        if (jugador1 != null)
        {
            if (jugador1.name == nombre)
                acelerarJugador1 = true;
            else
                acelerarJugador1 = false;
        }
        if (jugador2 != null)
        {
            if (jugador2.name == nombre)
                acelerarJugador2 = true;
            else
                acelerarJugador2 = false;
        }
    }
}
