using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Plataforma_Material : MonoBehaviour
{
    // cambia el color de la plataforma al colisionar con un jugador, se cambia al color del jugador que la choco
    Material mat;
    public bool ignorar;

    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;

        mat.SetInt("_Rojo_centro", Random.Range(0, 2));

        if (!ignorar)
            mat.SetFloat("_Rango", Random.Range(1, 11) / 10);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.name == "Jugador 1")
                mat.SetInt("_Rojo_centro", 1);
            else
                mat.SetInt("_Rojo_centro", 0);
        }
    }
}
