using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Piedra_Magnetica : MonoBehaviour
{
    // aplica fuersas verticales y horizontales (las ultimas con direccion aleatorea) cuando colicionan con un objeto
    public float inpulso, velocidad;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        rb.velocity = Vector3.zero;
        if (!(collision.gameObject.transform.position.y > transform.position.y))
            rb.AddForce(Vector3.up * inpulso, ForceMode.Impulse);
        if (Random.Range(0, 2) == 1)
            rb.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
        else
            rb.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
    }
}
