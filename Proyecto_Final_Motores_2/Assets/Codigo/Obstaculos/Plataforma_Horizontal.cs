using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.AudioSettings;

public class Plataforma_Horizontal : MonoBehaviour
{
    // mueve las las plataformas a velocidad constante, y camia su direccion cuando chocan conalgo que no sea un jugador. Tambienajusta su escala a un valor random
    Rigidbody rb;
    bool meMuevo;
    public float velocidad;
    Material mat;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (Random.Range(0, 2) == 1)
            meMuevo = true;
        if (Random.Range(0, 2) == 1)
            velocidad *= -1;
        transform.localScale += Vector3.right * Random.Range(5, 15);

        mat = GetComponent<MeshRenderer>().material;

        if (meMuevo)
            mat.SetInt("_Movil", 1);
        else
            mat.SetInt("_Movil", 0);
        mat.SetFloat("_Rango", Random.Range(1, 11) / 10);
    }

    private void Update()
    {
        if (meMuevo)
            rb.velocity = Vector3.right * velocidad;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
            velocidad *= -1;
    }
}
