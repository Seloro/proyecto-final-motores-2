using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Randomizador : MonoBehaviour
{
    // da valores aliatorios a diversas caracteristicas del juego
    public GameObject jugador1, jugador2, posocopnesIniciales, posicionesDeSectores;
    public List<GameObject> listaDeSectores;
    public SpriteRenderer Jugador1Sprit, ojo1Sprit, jugador2Sprit, ojo2Sprit;
    int sectorElejido, sectorUltimo;
    static public bool sectoresElejidos;
    public static int[] indisesElejidos;

    bool cambiarVista;

    private void Start()
    {
        sectorUltimo = -1;
        posicionamientoInicial();
        spritSortinOrder();
        creacionDeSectores();
    }

    private void Update()
    {
        cambioDeVistaJugadores();
    }

    void posicionamientoInicial()
    {
        int valorRandom;
        valorRandom = UnityEngine.Random.Range(0, 2);
        if (valorRandom == 0)
        {
            jugador1.transform.position = posocopnesIniciales.transform.GetChild(0).transform.position;
            jugador2.transform.position = posocopnesIniciales.transform.GetChild(1).transform.position;
        }
        else
        {
            jugador1.transform.position = posocopnesIniciales.transform.GetChild(1).transform.position;
            jugador2.transform.position = posocopnesIniciales.transform.GetChild(0).transform.position;
        }
    }// determina que jugador inicia arriva y cual avajo

    void cambioDeVistaJugadores()// Determina cuando se ejecuta el metodo "spritSortinOrder"
    {
        if (math.abs(jugador1.transform.position.x - jugador2.transform.position.x) >= 10)
            cambiarVista = true;
        else if (cambiarVista)
        {
            spritSortinOrder();
            cambiarVista = false;
        }
    }

    void spritSortinOrder()// Cambia cual jugador se muestra sobre el otro
    {
        int valorRandom;
        valorRandom = UnityEngine.Random.Range(0, 2);
        if (valorRandom == 0)
        {
            Jugador1Sprit.sortingOrder = 1;
            ojo1Sprit.sortingOrder = 2;
            jugador2Sprit.sortingOrder = 3;
            ojo2Sprit.sortingOrder = 4;
        }
        else
        {
            Jugador1Sprit.sortingOrder = 3;
            ojo1Sprit.sortingOrder = 4;
            jugador2Sprit.sortingOrder = 1;
            ojo2Sprit.sortingOrder = 2;
        }
    }

    void creacionDeSectores()// Elije que sectores apareseran en la pista, no puede elejir dos veces seguidas el mismo
    {
        if (sectoresElejidos)
            sectoresElejidosMetodo();
        else
            sectoresAleatorios();
    }

    void sectoresAleatorios()
    {
        for (int i = 0; i < posicionesDeSectores.transform.childCount; i++)
        {
            do
            {
                sectorElejido = UnityEngine.Random.Range(0, listaDeSectores.Count);
            } while (sectorElejido == sectorUltimo);
            Instantiate(listaDeSectores[sectorElejido], posicionesDeSectores.transform.GetChild(i).transform);
            sectorUltimo = sectorElejido;
        }
    }

    void sectoresElejidosMetodo()
    {
        for (int i = 0; i < posicionesDeSectores.transform.childCount; i++)
        {
            Instantiate(listaDeSectores[indisesElejidos[i]], posicionesDeSectores.transform.GetChild(i).transform);
        }
    }
}
