using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    // Detecta y pasa el nombre del jugador que pasa la meta al GameManager
    bool primero;
    private void OnTriggerExit(Collider other)
    {
        if (!primero && other.gameObject.transform.position.x > transform.position.x)
        {
            Game_Manager.nombre = other.gameObject.name;
            primero = true;
        }
    }
}
