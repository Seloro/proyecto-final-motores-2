using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador_De_Plataformas : MonoBehaviour
{
    //Genera plataformas en posiciones del eje x aleatores destro de un ranco dado. las plataformas de instancian en los hijos del game objet que tenga el codigo
    public GameObject plataforma;

    private void Start()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            Instantiate(plataforma, this.transform.GetChild(i).transform).transform.position += Vector3.right * Random.Range(-20, 21);
        }
    }
}
