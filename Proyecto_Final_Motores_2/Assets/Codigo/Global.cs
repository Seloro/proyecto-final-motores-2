using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Global : MonoBehaviour
{
    public GameObject jugador1, jugador2;
    public Comando_Jugador comandoJugdor1, comandoJugador2;
    public float limiteVelocidadBase, velocidadBase, distanciaDeRezagado, aumentoVelocidadRezagado, aumentoVelocidadPlataforma;
    float aumentoVelocidadRezagado1, aumentoVelocidadRezagado2, aumentoVelocidadPlataforma1, aumentoVelocidadPlataforma2;

    void Update()
    {
        controlVelocidades();
        rezagado();
        aumentoPorPlataforma();
    }

    void controlVelocidades()
    {
        comandoJugdor1.limiteVelocidad = limiteVelocidadBase + aumentoVelocidadRezagado1 + aumentoVelocidadPlataforma1;
        comandoJugador2.limiteVelocidad = limiteVelocidadBase + aumentoVelocidadRezagado2 + aumentoVelocidadPlataforma2;
        comandoJugdor1.velocidad = velocidadBase + aumentoVelocidadRezagado1 + aumentoVelocidadPlataforma1;
        comandoJugador2.velocidad = velocidadBase + aumentoVelocidadRezagado2 + aumentoVelocidadPlataforma2;
    }

    void rezagado()
    {
        if (math.abs(jugador1.transform.localPosition.x - jugador2.transform.localPosition.x) > distanciaDeRezagado)
        {
            if (jugador1.transform.localPosition.x < jugador2.transform.localPosition.x)
                aumentoVelocidadRezagado1 = aumentoVelocidadRezagado;
            else
                aumentoVelocidadRezagado2 = aumentoVelocidadRezagado;
        }
        else
        {
            aumentoVelocidadRezagado1 = 0;
            aumentoVelocidadRezagado2 = 0;
        }
    }

    void aumentoPorPlataforma()
    {
        if (Plataforma_Rapida.acelerarJugador1)
            aumentoVelocidadPlataforma1 = aumentoVelocidadPlataforma;
        else
            aumentoVelocidadPlataforma1 = 0;
        if (Plataforma_Rapida.acelerarJugador2)
            aumentoVelocidadPlataforma2 = aumentoVelocidadPlataforma;
        else
            aumentoVelocidadPlataforma2 = 0;
    }
}
