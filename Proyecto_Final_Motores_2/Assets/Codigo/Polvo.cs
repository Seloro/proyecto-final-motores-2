using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polvo : MonoBehaviour
{
    ParticleSystem par;
    void Start()
    {
        par = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (par.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
