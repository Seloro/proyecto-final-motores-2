using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.UI;

public class Comando_Jugador : MonoBehaviour
{
    // gestiona casi todo lo relacionado con el jugador
    CapsuleCollider coll;
    Rigidbody rb;
    SpriteRenderer sprite;
    public float velocidad, limiteVelocidad, inpulso, distanciaRayCast, velocidadBala;
    public KeyCode derecha, izquierda, salto, bajar, atacarTecla;
    public LayerMask jugador, plataformas, noIgnorar, caida, ignorarRaycast, raycastCaida, raycastSalto;
    Animator animacion;
    bool enPiso, invertirOjo, cambioPermitido, da�ado, caidaAudio, saltoAudio, caminarAudio;
    public GameObject ojo;
    float tempDa�o, tempAtaque;
    public TextMeshProUGUI textVelocidad, textSector, textAtaque;
    int sector, indise;
    PhysicMaterial rozamiento;
    public bool disparar, ataque;
    public GameObject bala, mira;
    ParticleSystem pelo;

    void Start()
    {
        coll = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
        sprite = GetComponent<SpriteRenderer>();
        animacion = GetComponent<Animator>();
        cambioPermitido = true;
        if (gameObject.name == "jugador 1")
            indise = 1;
        else
            indise = 2;
        rozamiento = GetComponent<CapsuleCollider>().material;
        pelo = GetComponent<ParticleSystem>();
    }

    void FixedUpdate()
    {
        input();
    }

    private void Update()
    {
        saltarYBajar();
        controlDeAnimacion();
        repocisionamientoComponentes();
        cambiDeColiciones();
        limiteDeVelocidad();
        controlDeDa�o();
        controlDeTexto();
        controlSonido();
        atacar();
        cargaDeAtaque();
    }

    private void OnCollisionEnter(Collision collision)// si el jugador coliciona contra el otro o un obstaculo lo detiene. en caso de colisionar con un jugador y ser el de arviba enves de detenerlo se le aplica una fuersa para simular un rebote
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.transform.position.y < transform.position.y)
            {
                rb.AddForce(Vector3.up * 2.5f, ForceMode.Impulse);
                if (rb.velocity.x < 0)
                    rb.AddForce(Vector3.left * 2.5f, ForceMode.Impulse);
                else
                    rb.AddForce(Vector3.right * 2.5f, ForceMode.Impulse);
            }
            else
            {
                rb.velocity = Vector3.zero;
                tempDa�o = 1f;
                GestorDeAudio.instancia.ReproducirSonido("Da�o" + indise);
                pelo.Play();
            }
        }
        if (collision.gameObject.CompareTag("Obstaculo") || collision.gameObject.CompareTag("Bala") || collision.gameObject.CompareTag("Muro"))
        {
            rb.velocity = Vector3.zero;
            if (collision.gameObject.transform.position.x > transform.position.x)
                rb.AddForce(Vector3.left * 2.5f, ForceMode.Impulse);
            else
                rb.AddForce(Vector3.right * 2.5f, ForceMode.Impulse);
            tempDa�o = 1f;
            GestorDeAudio.instancia.ReproducirSonido("Da�o" + indise);
            pelo.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Divicion"))
        {
            if (other.gameObject.transform.position.x < transform.position.x)
            {
                if (other.gameObject.name == "1")
                    sector = 1;
                else if (other.gameObject.name == "2")
                    sector = 2;
                else if (other.gameObject.name == "3")
                    sector = 3;
                else if (other.gameObject.name == "4")
                    sector = 4;
                else if (other.gameObject.name == "5")
                    sector = 5;
            }
            else
            {
                if (other.gameObject.name == "1")
                    sector = 0;
                else if (other.gameObject.name == "2")
                    sector = 1;
                else if (other.gameObject.name == "3")
                    sector = 2;
                else if (other.gameObject.name == "4")
                    sector = 3;
                else if (other.gameObject.name == "5")
                    sector = 4;
            }
        }
    }// Detecta el sector en el que se encuentre en funcion de coliciones con eventos trigger

    void input()
    {
        if (rb.velocity.y < .5f && rb.velocity.y > -.5f && !da�ado && !ataque)
        {
            if (Input.GetKey(derecha))
            {
                rb.AddForce(Vector3.right * velocidad, ForceMode.Acceleration);
                sprite.flipX = false;
            }
            else if (Input.GetKey(izquierda))
            {
                rb.AddForce(Vector3.left * velocidad, ForceMode.Acceleration);
                sprite.flipX = true;
            }
        }
    }// movimiento en el eje x siempre que no se mueva en el eje y o este da�ado

    void saltarYBajar()
    {
        if (!da�ado && !ataque)
        {
            if (Input.GetKeyDown(salto) && Physics.Raycast(transform.position, Vector3.down, distanciaRayCast))
            {
                cambioPermitido = true;
                if (!Physics.Raycast(transform.position, Vector3.down, distanciaRayCast, ignorarRaycast))
                {
                    rb.AddForce(Vector3.up * inpulso, ForceMode.Impulse);
                    saltoAudio = true;
                }
            }
            else if (Input.GetKey(bajar) && Physics.Raycast(transform.position, Vector3.down, distanciaRayCast, plataformas))
            {
                rb.excludeLayers = plataformas;
                cambioPermitido = false;
                rb.AddForce(Vector3.down * .25f, ForceMode.Impulse);
            }
            else
                cambioPermitido = true;
        }
    }// permite salta o bajar de plataforms siempre que no se este da�ado

    void controlDeAnimacion()
    {
        animacion.SetFloat("VelocidadX", math.abs(rb.velocity.x));
        animacion.SetFloat("VelocidadY", rb.velocity.y);
        animacion.SetBool("EnPiso", enPiso);
        animacion.SetBool("Da�o", da�ado);
        //animacion.SetBool("Ataque", ataque);

        if (Physics.Raycast(transform.position, Vector3.down, distanciaRayCast) && !da�ado)
        {
            if (!Physics.Raycast(transform.position, Vector3.down, distanciaRayCast, ignorarRaycast))
            {
                enPiso = true;
                rozamiento.dynamicFriction = .6f;
            }
        }
        else
        {
            enPiso = false;
            caidaAudio = true;
            rozamiento.dynamicFriction = 0;
        }
    }// Vincula variables del jugador con las del animator que jestiona las animacionesdel jugador

    void repocisionamientoComponentes()
    {
        if (da�ado || ataque)
            ojo.gameObject.SetActive(false);
        else
        {
            if (!enPiso && math.abs(rb.velocity.y) > 2)
                ojo.gameObject.SetActive(false);
            else if (enPiso)
                ojo.gameObject.SetActive(true);
        }
        if (sprite.flipX && invertirOjo)
        {
            ojo.gameObject.transform.localPosition = new Vector3(-ojo.gameObject.transform.localPosition.x, ojo.gameObject.transform.localPosition.y, ojo.gameObject.transform.localPosition.z);
            coll.center = new Vector3(-coll.center.x, coll.center.y, coll.center.z);
            mira.transform.localPosition = new Vector3(-mira.transform.localPosition.x, mira.transform.localPosition.y, mira.transform.localPosition.z);
            invertirOjo = false;
        }
        else if (!sprite.flipX)
        {
            ojo.gameObject.transform.localPosition = new Vector3(math.abs(ojo.gameObject.transform.localPosition.x), ojo.gameObject.transform.localPosition.y, ojo.gameObject.transform.localPosition.z);
            coll.center = new Vector3(math.abs(coll.center.x), coll.center.y, coll.center.z);
            mira.transform.localPosition = new Vector3(math.abs(mira.transform.localPosition.x), mira.transform.localPosition.y, mira.transform.localPosition.z);
            invertirOjo = true;
        }
    }// eajusta la posicion del ojo y colider

    void cambiDeColiciones()
    {
        if (rb.velocity.y != 0 && cambioPermitido)
        {
            if (rb.velocity.y > .2f)
            {
                rb.excludeLayers = plataformas;
                ignorarRaycast = raycastSalto;
            }
            else if (rb.velocity.y < -.2f)
            {
                rb.excludeLayers = noIgnorar;
                ignorarRaycast = raycastCaida;
                gameObject.layer = 7;
            }
        }
        else if (cambioPermitido)
        {
            rb.excludeLayers = jugador;
            gameObject.layer = 3;
        }
    }// cambia los layer con los que puede o no colicionar segun diversas situaciones

    void limiteDeVelocidad()
    {
        if (rb.velocity.x > limiteVelocidad)
        {
            rb.velocity = new Vector3(limiteVelocidad, rb.velocity.y, rb.velocity.z);
        }
        else if (rb.velocity.x < -limiteVelocidad)
        {
            rb.velocity = new Vector3(-limiteVelocidad, rb.velocity.y, rb.velocity.z);
        }
    }// limita la velocidad

    void controlDeDa�o()
    {
        if (tempDa�o <= 0)
            da�ado = false;
        else
        {
            tempDa�o -= Time.deltaTime;
            da�ado = true;
        }
    }//Cuenta regresiva para saber cuando se recupera del da�o

    void controlDeTexto()
    {
        textAtaque.text = "Atq: " + ((int)tempAtaque) + "%";
        if ((sector <= 0) || (sector >= 5))
            textSector.text = "S -";
        else
            textSector.text = "S " + sector;
        textVelocidad.text = "V: " + (math.abs((int)rb.velocity.x) * 2) + "/" + ((int)limiteVelocidad * 2);
    }// reajusta los textos de velocidad y sector actual

    void controlSonido()
    {
        if (saltoAudio && rb.velocity.y > 0)
        {
            GestorDeAudio.instancia.ReproducirSonido("Salto" + indise);
            saltoAudio = false;
        }

        if (caidaAudio && rb.velocity.y == 0 && !da�ado)
        {
            GestorDeAudio.instancia.ReproducirSonido("Caida" + indise);
            caidaAudio = false;
        }
    }// determina cuando y que sonido reproducir

    void atacar()
    {
        if (Input.GetKeyDown(atacarTecla) && !ataque && enPiso && !da�ado && (tempAtaque == 100))
        {
            rb.velocity = Vector3.zero;
            animacion.SetTrigger("Ataque");
        }
        if (disparar && tempAtaque == 100)
        {
            GameObject balaActual = Instantiate(bala, mira.transform.position, Quaternion.identity);
            Rigidbody rbBala = balaActual.GetComponent<Rigidbody>();
            if (mira.transform.localPosition.x > 0)
            {
                rbBala.AddForce(Vector3.right * velocidadBala, ForceMode.Impulse);
                rbBala.AddForce(Vector3.up * 2, ForceMode.Impulse);
            }
            disparar = false;
            tempAtaque = 0;
        }
    }

    void cargaDeAtaque()
    {
        if (sector > 0 && sector < 5)
        {
            if (rb.velocity.magnitude > 0)
            {
                if (tempAtaque < 100)
                    tempAtaque += Time.deltaTime * rb.velocity.magnitude;
                else
                    tempAtaque = 100;
            }
        }
    }
}
