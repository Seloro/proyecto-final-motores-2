using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public int indice;
    int indisePropio;
    static public List<GameObject> balasActualesUno = new List<GameObject>();
    static public List<GameObject> balasActualesDos = new List<GameObject>();
    void Start()
    {
        if (indice == 1)
        {
            balasActualesUno.Add(gameObject);

            if (balasActualesUno.Count > 4)
            {
                Destroy(balasActualesUno[0]);
                balasActualesUno.Remove(balasActualesUno[0]);
            }
            indisePropio = balasActualesUno.Count - 1;
        }
        else
        {
            balasActualesDos.Add(gameObject);

            if (balasActualesDos.Count > 4)
            {
                Destroy(balasActualesDos[0]);
                balasActualesDos.Remove(balasActualesDos[0]);
            }
            indisePropio = balasActualesDos.Count - 1;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Bala") || collision.gameObject.CompareTag("Muro"))
        {
            if (indice == 1)
            {
                Destroy(balasActualesUno[indisePropio]);
                balasActualesUno.Remove(balasActualesUno[indisePropio]);
            }
            else
            {
                Destroy(balasActualesDos[indisePropio]);
                balasActualesDos.Remove(balasActualesDos[indisePropio]);
            }
        }
    }
}
