using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ojo : MonoBehaviour
{
    //Activa la animacion de parpadeo del ojo cada interbalo de 0.5 a 4 segundos. tambien reajusta el flip en eje x del sprite del ojo
    Animator animacionOjo;
    float temp, limite;
    bool parpadear;
    SpriteRenderer ojo;

    void Start()
    {
        animacionOjo = GetComponent<Animator>();
        limite = UnityEngine.Random.Range(1, 4);
        ojo = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        parpadeo();
        reposicionamiento();
        animacionOjo.SetBool("Parpadear", parpadear);
        //print(temp + " " + limite);
    }

    void temporizador()
    {
        temp += Time.deltaTime;
        if (temp > limite)
        {
            if (parpadear)
                limite = .5f;
            else
                limite = UnityEngine.Random.Range(.5f, 3.5f);
            temp = 0;
            parpadear = !parpadear;
        }
    }

    void parpadeo()
    {
        if (gameObject.activeSelf)
            temporizador();
        else
        {
            if (parpadear)
            {
                parpadear = false;
                limite = UnityEngine.Random.Range(.5f, 3.5f);
            }
            temp = 0;
        }
    }

    void reposicionamiento()
    {
        if (transform.localPosition.x < 0)
            ojo.flipX = true;
        else
            ojo.flipX = false;
    }
}
