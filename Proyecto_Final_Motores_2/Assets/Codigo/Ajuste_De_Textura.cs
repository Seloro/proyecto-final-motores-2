using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ajuste_De_Textura : MonoBehaviour
{
    // Reajusta la cantidad de veces que se repite una textura en funcion de la escala del objeo
    Material mat;

    private void Start()
    {
        mat = GetComponent<MeshRenderer>().material;

        mat.SetVector("_Tiling", new Vector2(transform.localScale.x, transform.localScale.y));
    }
}
