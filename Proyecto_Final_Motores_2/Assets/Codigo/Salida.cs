using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Salida : MonoBehaviour 
{
    //Cunta conun temporisador y un texto que muestra una cuenta regresiva de 3 segundos para indicar el inicio de la carera
    public TextMeshProUGUI contadorDeInicio;
    float tempInicio;
    Rigidbody rb;
    Vector3 posicionFinal;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        tempInicio = 4;
        posicionFinal = transform.position + Vector3.forward;
    }

    void FixedUpdate()
    {
        abrirPuerta();
    }

    void Update()
    {
        temporizador();
        cuentaRegresiba();
    }

    void abrirPuerta()
    {
        if (tempInicio <= 0)
        {
            rb.MovePosition(posicionFinal);
        }
    }

    void temporizador()
    {
        if (tempInicio > 0)
        {
            tempInicio -= Time.deltaTime;
        }
    }
    void cuentaRegresiba()
    {
        if (tempInicio <= 0)
        {
            contadorDeInicio.gameObject.SetActive(false);
        }
        else if (tempInicio <= 1)
        {
            contadorDeInicio.text = "1";
        }
        else if (tempInicio <= 2)
        {
            contadorDeInicio.text = "2";
        }
        else
        {
            contadorDeInicio.text = "3";
        }
    }
}
